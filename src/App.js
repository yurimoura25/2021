import "./App.css";
import "./styles.css";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import PessoaFormulario from './views/Pessoa/PessoaFormulario.js';
import PessoaLista from './views/Pessoa/PessoaLista.js';
import Home from './Home.js';
import NotFound from './NotFound.js';


import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Nav from "react-bootstrap/Nav";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import {
  faShoppingCart,
  faUserCircle,
} from "@fortawesome/free-solid-svg-icons";

function App() {
  return (
    <div className="App">
      <Router>
        <header className="bg-blue">
          <Container>
            <Row>
              <Col xs lg="2">
                <img
                  id="logo"
                  alt="logo do site"
                  src={`${process.env.PUBLIC_URL}/img/logo.svg`}
                />
              </Col>
              <Col xs lg="auto">
                <Nav>
                  <Nav.Item >
                    <Nav.Link href="/" className="color-white">Home</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link href="/produtos" className="color-white">Produtos</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link href="/contatos" className="color-white">Contato</Nav.Link>
                  </Nav.Item>
                </Nav>
              </Col>
              <Col xs lg="1" className="shopping-status">
                <FontAwesomeIcon
                  className="shopping-cart"
                  icon={faShoppingCart}
                />
                <span className="shopping-count">0 itens</span>
              </Col>
              <Col xs lg="1">
                <FontAwesomeIcon className="user-login" icon={faUserCircle} />
              </Col>
            </Row>
          </Container>
        </header>
        <main className="content text-center">
          <Container>
            <Row>
              <Col xs={3}>
                <Nav defaultActiveKey="/" className="flex-column menu-vertical">
                  <Link to="/" className="bg-light-gray nav-link">
                    Home
                  </Link>
                  <Link to="/pessoa/lista" className="bg-light-gray nav-link">
                    Pessoa
                  </Link>
                </Nav>
              </Col>
              <Col xs={9}>
                <Switch>
                  <Route exact path="/">
                    <Home />
                  </Route>
                  <Route path="/pessoa/lista"><PessoaLista /></Route>
                  <Route exact path="/pessoa/formulario/"><PessoaFormulario /></Route>
                  <Route exact path="/pessoa/formulario/:id"><PessoaFormulario /></Route>
                  <Route path="*">
                    <NotFound />
                  </Route>
                </Switch>
              </Col>
            </Row>
          </Container>
        </main>
        <footer className="bg-blue">
          <Container>
            <Row className="d-flex align-items-end text-center">
              <Col>Facilita © - Todos direitos reservados 2021</Col>
            </Row>
          </Container>
        </footer>
      </Router>
    </div>
  );
}

export default App;
