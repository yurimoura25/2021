
import axios from 'axios';

const URL = "http://localhost:3004"

const listar = () => {

    return axios.get(`${URL}/pessoas`)
}

const buscarPeloId = (id) => {
    if(id!= undefined) {
    return axios.get(`${URL}/pessoas/`+ id)
    }
}

const salvar = (pessoa) => {
    if(pessoa.id == undefined) {
        return axios.post(`${URL}/pessoas/`, pessoa)
    }
    else {
        return axios.put(`${URL}/pessoas/${pessoa.id}`, pessoa)
    }
}

const excluir = (id) => {
    return axios.delete(`${URL}/pessoas/${id}`)
}

export default {
    listar,
    buscarPeloId,
    salvar,
    excluir
}