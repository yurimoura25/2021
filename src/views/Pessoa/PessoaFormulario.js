import React, {useState, useEffect} from "react";
import { Formik, Form, Field, ErrorMessage } from 'formik';

import PessoaService from "../../service/PessoaService";

import {useHistory,
        useParams  } from 'react-router-dom';

function PessoaFormulario() {

  let history = useHistory();

  let {id} = useParams();

  const [pessoaState, setPessoaState] = useState({});

  const buscarPessoa = () =>{
    if(id!=undefined) {
      PessoaService.buscarPeloId(id)
      .then(response => {
        setPessoaState(response.data);
      })
      .catch(error => console.log(`error:${error}`))
    }
  }
  useEffect(() => {
    buscarPessoa()
  }, [])

  const handleSubmit = (( values, {setSubmitting}) => {
    console.log(values);
    PessoaService.salvar(values)
    .then(response => {
      setSubmitting(false);
      history.push('/pessoa/lista')

    }).catch(error => {console.log(`error: ${error}`)})
      setSubmitting(false);

  });

  const mapStateToObject = () => {
    return {
      id: pessoaState.id || undefined,
      nome: pessoaState.nome || "",
      cpf: pessoaState.cpf || "",
      img: pessoaState.img || ""
    }
  }

  return (
    <div>
      <h1>Cadastro de Pessoas</h1>
      <Formik
        initialValues= {mapStateToObject()}
        enableReinitialize
        onSubmit={ handleSubmit }

      >

        { ({values , handleSubmit,isSubmitting}) => (
        <Form onSubmit={handleSubmit}>
          <div className="input-group mb-3">
          <Field 
              name="img"
              type="file" 
              className="form-control"
              />
          </div>

          <div className="mb-3 text-start">
            <label htmlFor="exampleInputEmail1" className="form-label">
              Nome
            </label>
            <Field 
              name="nome"
              type="text" 
              className="form-control"/>
            <div id="emailHelp" className="form-text"></div>
          </div>
          <div className="mb-3 text-start">
            <label htmlFor="exampleInputPassword1" className="form-label">
              CPF
            </label>
            <Field 
              name="cpf"
              type="text" 
              className="form-control"/>
          </div>
          <button type="submit" className="btn btn-primary" disabled={isSubmitting}>
            Salvar
          </button>
        </Form>
      )}
      </Formik>
    </div>
  );
}

export default PessoaFormulario;
