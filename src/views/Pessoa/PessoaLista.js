import React, { useEffect, useState } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import "../../styles.css";


import PessoaService from "../../service/PessoaService"

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import {
  faPencilAlt,
  faTrashAlt,
  faSearch,
} from "@fortawesome/free-solid-svg-icons";

import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";

function PessoaLista() {
  
  /* Hooks*/
  useEffect(() => {
    listar();
  }, [])

  const [showState, setShowState] = useState(false);
  const [pessoaListaState, setPessoasListaState] = useState([]);
  const [searchState, setSearchState] = useState('');
  const [pessoaItemState, setPessoaItemState] = useState({})

  const handleClose = () => setShowState(false);
  const handleShow = (pessoa) => {setPessoaItemState(pessoa); setShowState(true)};
  const handleSearch = (event) => setSearchState(event.target.value);
  const handleExcluir = () => { 
    PessoaService.excluir(pessoaItemState.id)
    .then((response) => {

      const listaAtualizada = pessoaListaState.filter(filterExcluir);
      setPessoasListaState(listaAtualizada);
      console.log("response:", response);
    })
    .catch(error => {console.log('error: ', error)})
    setShowState(false)
  };

  const filterSearch = (pessoa) => {
    return (pessoa.nome.toLowerCase().includes(searchState.toLowerCase()) ||
    pessoa.cpf.toLowerCase().includes(searchState.toLowerCase()));
  }
  const filterExcluir = (pessoa) => {
    return pessoa.id != pessoaItemState.id;
  } 

  const listar = () => {
    PessoaService.listar()
      .then(response => {
        setPessoasListaState(response.data)
      })
      .catch(error => {console.log("error:", error)})

  }

  return (
    <div>
      <h1>Lista de Pessoas</h1>
      <div className="container">
        <div className="row mtb-10">
          <div className="col-sm text-align-left mlr-n10">
            <Link to="/pessoa/formulario" className="btn btn-primary">
              Novo
            </Link>
          </div>
          <div className="col-sm text-align-right mlr-n10">
            <div className="input-group flex-nowrap">
              <input
                type="text"
                className="form-control"
                placeholder="Username"
                aria-label="Username"
                aria-describedby="addon-wrapping"
                onChange={handleSearch}
              />
              <span className="input-group-text" id="addon-wrapping">
                <FontAwesomeIcon icon={faSearch} />
              </span>
            </div>
          </div>
        </div>
      </div>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nome</th>
            <th scope="col">CPF</th>
            <th scope="col">Operações</th>
          </tr>
        </thead>
        <tbody>
          {pessoaListaState
          .filter(filterSearch)
          .map((pessoa, index) => {
            return (
              <tr key={index}>
                <th scope="row">{pessoa.id}</th>
                <td>{pessoa.nome}</td>
                <td>{pessoa.cpf}</td>
                <td>
                  <Link to={`/pessoa/formulario/${pessoa.id}`} >
                  <Button variant="outline-primary" className="mlr-10">
                    <FontAwesomeIcon
                      className="pessoa-alterar"
                      icon={faPencilAlt}
                    />
                  </Button>
                  </Link>
                  <Button variant="outline-primary" onClick={()=> { handleShow(pessoa) }}>
                    <FontAwesomeIcon
                      className="pessoa-deletar "
                      icon={faTrashAlt}
                    />
                  </Button>
                </td>
            </tr>
            )
          })}
        
        </tbody>
      </table>
      <Modal show={showState} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Confirmar Exclusão</Modal.Title>
        </Modal.Header>
        <Modal.Body>Deseja realmente excluir {pessoaItemState.nome}? </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cancelar
          </Button>
          <Button variant="danger" onClick={handleExcluir}>
            Excluir
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

export default PessoaLista;
